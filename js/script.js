
  var c = document.getElementById("myCanvas");
  var ctx = c.getContext("2d");
  ctx.moveTo(50, 350);
  ctx.lineTo(50, 150);
  ctx.lineTo(30, 150);
  ctx.lineTo(120, 40);
  ctx.lineTo(210, 150);
  ctx.lineTo(190, 150);
  ctx.lineTo(190, 350);
  ctx.lineTo(50, 350);
  ctx.stroke();

  ctx.beginPath();
  ctx.moveTo(130, 50);
  ctx.lineTo(450, 50);
  ctx.lineTo(500, 140);
  ctx.lineTo(203, 140);
  ctx.fillStyle = "#D35400";
  ctx.fill();
  ctx.stroke();

  ctx.beginPath();
  ctx.moveTo(190, 340);
  ctx.lineTo(480, 340);
  ctx.lineTo(480, 140);
  ctx.stroke();

  ctx.beginPath();
  ctx.moveTo(70, 150);
  ctx.lineTo(70, 200);
  ctx.lineTo(110, 200);
  ctx.lineTo(110, 150);
  ctx.lineTo(70, 150);
  ctx.fillStyle = "#85C1E9";
  ctx.fill();
  ctx.stroke();

  ctx.beginPath();
  ctx.moveTo(130, 150);
  ctx.lineTo(130, 200);
  ctx.lineTo(170, 200);
  ctx.lineTo(170, 150);
  ctx.lineTo(130, 150);
  ctx.fillStyle = "#85C1E9";
  ctx.fill();
  ctx.stroke();

  ctx.beginPath();
  ctx.moveTo(70, 220);
  ctx.lineTo(70, 270);
  ctx.lineTo(110, 270);
  ctx.lineTo(110, 220);
  ctx.lineTo(70, 220);
  ctx.fillStyle = "#85C1E9";
  ctx.fill();
  ctx.stroke();

  ctx.beginPath();
  ctx.moveTo(130, 220);
  ctx.lineTo(130, 270);
  ctx.lineTo(170, 270);
  ctx.lineTo(170, 220);
  ctx.lineTo(130, 220);
  ctx.fillStyle = "#85C1E9";
  ctx.fill();
  ctx.stroke();

  ctx.beginPath();
  ctx.moveTo(210, 180);
  ctx.lineTo(260, 180);
  ctx.lineTo(260, 280);
  ctx.lineTo(210, 280);
  ctx.lineTo(210, 180);
  ctx.stroke();

  ctx.beginPath();
  ctx.moveTo(460, 180);
  ctx.lineTo(410, 180);
  ctx.lineTo(410, 280);
  ctx.lineTo(460, 280);
  ctx.lineTo(460, 180);
  ctx.stroke();

  ctx.beginPath();
  ctx.moveTo(430, 50);
  ctx.lineTo(430, 20);
  ctx.lineTo(390, 20);
  ctx.lineTo(390, 50);
  ctx.stroke();

  ctx.beginPath();
  ctx.moveTo(280, 340);
  ctx.lineTo(280, 240);
  ctx.lineTo(290, 240);
  ctx.lineTo(290, 340);
  ctx.stroke();

  ctx.beginPath();
  ctx.moveTo(380, 340);
  ctx.lineTo(380, 240);
  ctx.lineTo(390, 240);
  ctx.lineTo(390, 340);
  ctx.stroke();

  ctx.beginPath();
  ctx.moveTo(300, 340);
  ctx.lineTo(300, 240);
  ctx.lineTo(370, 240);
  ctx.lineTo(370, 340);
  ctx.fillStyle = "#F4D03F";
  ctx.fill();
  ctx.stroke();

  toDrawDetails();


function toDrawDetails() {
  var c = document.getElementById("myCanvas");
  var ctx = c.getContext("2d");

  //rigth window's details

  ctx.beginPath();
  ctx.moveTo(210, 200);
  ctx.lineTo(260, 200);
  ctx.stroke();

  ctx.beginPath();
  ctx.moveTo(210, 220);
  ctx.lineTo(260, 220);
  ctx.stroke();

  ctx.beginPath();
  ctx.moveTo(210, 240);
  ctx.lineTo(260, 240);
  ctx.stroke();

  ctx.beginPath();
  ctx.moveTo(210, 260);
  ctx.lineTo(260, 260);
  ctx.stroke();

  ctx.beginPath();
  ctx.moveTo(235, 280);
  ctx.lineTo(235, 200);
  ctx.stroke();

  //left window's details

  ctx.beginPath();
  ctx.moveTo(410, 200);
  ctx.lineTo(460, 200);
  ctx.stroke();

  ctx.beginPath();
  ctx.moveTo(410, 220);
  ctx.lineTo(460, 220);
  ctx.stroke();

  ctx.beginPath();
  ctx.moveTo(410, 240);
  ctx.lineTo(460, 240);
  ctx.stroke();

  ctx.beginPath();
  ctx.moveTo(410, 260);
  ctx.lineTo(460, 260);
  ctx.stroke();

  ctx.beginPath();
  ctx.moveTo(435, 280);
  ctx.lineTo(435, 200);
  ctx.stroke();

  ctx.beginPath();
  ctx.arc(335, 240, 55, Math.PI, 0);
  ctx.stroke();

  ctx.beginPath();
  ctx.arc(335, 240, 48, Math.PI, 0);
  ctx.stroke();

  ctx.beginPath();
  ctx.arc(530, 230, 30, Math.PI * 2, 0);
  ctx.fillStyle = "#2ECC71";
  ctx.fill();
  ctx.stroke();

  ctx.beginPath();
  ctx.arc(560, 290, 25, Math.PI * 2, 0);
  ctx.fillStyle = "#229954";
  ctx.fill();
  ctx.stroke();

  ctx.beginPath();
  ctx.arc(520, 330, 20, Math.PI * 2, 0);
  ctx.fillStyle = "#196F3D";
  ctx.fill();
  ctx.stroke();

}
